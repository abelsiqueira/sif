This directory contains the official CUTEst set of optimization
test problems expressed in Standard Input Format (SIF). Each example
is contained in a file with the extension .SIF. The leading comments
describe briefly the source of the example, with citations if relevant,
and contains a classification string that conforms to rules described in

  http://www.cuter.rl.ac.uk//Problems/classification.shtml

For a formal definition of SIF, see

  http://www.numerical.rl.ac.uk/lancelot/sif/sifhtml.html

In addition, for convenience, there are subdirectories that provide
symbolic links to all the CUTEst examples in commonly-occurring subsets.
These are

 lpsif   Linear Programming examples
 qpsif   Quadratic Programming examples
 bqpsif  Bound-constrained Quadratic Programming examples
 nlssif  Nonlinear least-squares examples
 nlesif  Nonlinear equation and inequality examples
 uncsif  Unconstrained examples
 bcsif   Bound-Constrained examples
 nlpsif  Nonlinear Programming examples

Each directory contains a file *.dat listing the examples in alphabetical order.

Further subsets may be found using the SIFDecode select tool from

  https://github.com/ralna/SIFDecode/wiki

Nick Gould, for GALAHAD productions
2nd March, 2018
